unit SymlessCloudV1Tests;

{$mode objfpc}{$H+}
{$APPTYPE CONSOLE}

interface

uses
   Classes,
   SysUtils,
   //fpcunit,
   //testutils,
   //testregistry,
   TestFramework,
   Windows,
   httpsend,
   ssl_openssl;

type

   { TTestCaseSynergyCloudV1 }

   TTestCaseSynergyCloudV1 = class(TTestCase)
   published
      procedure TestQueryUpdate;
      procedure TestQueryProfileSwitch;
      procedure TestQueryProfileUpdate;
      procedure TestQueryScreenUpdate;
   end;

implementation

procedure TTestCaseSynergyCloudV1.TestQueryUpdate;
var
   h: THTTPSend;
   sl: TStringList;
   b: boolean;
begin
   h := THTTPSend.Create;
   sl := TStringList.Create;
   sl.Add('{ "currentVersion": "2.0.1-stable" }');
   try
      h.Document.Seek(0, soFromBeginning);
      h.Document.SetSize(0);
      sl.SaveToStream(h.Document);
      Status('https://v1.api.cloud.symless.com/update');
      Status(sl.Text);
      b := h.HTTPMethod('POST', 'https://v1.api.cloud.symless.com/update');
      CheckTrue(b, 'POST https://v1.api.cloud.symless.com/update');
      if b then begin
         sl.Clear;
         h.Document.Seek(0, soFromBeginning);
         sl.LoadFromStream(h.Document);
         Status(sl.Text);
      end;
      CheckEquals(200, h.ResultCode);
   finally
      h.Free;
      sl.Free;
   end;
end;

procedure TTestCaseSynergyCloudV1.TestQueryProfileSwitch;
var
   h: THTTPSend;
   sl: TStringList;
   b: boolean;
begin
   h := THTTPSend.Create;
   sl := TStringList.Create;
   sl.Add('{"profile": { "id": 25, "name": "default" }, "screens": [ { "id": 2062, "x_pos": 260, "y_pos": 161 }, { "id": 31, "x_pos": 346, "y_pos": 162 } ], "version": 15}');
   try
      h.Document.Seek(0, soFromBeginning);
      h.Document.SetSize(0);
      sl.SaveToStream(h.Document);
      Status('https://v1.api.cloud.symless.com/profile/switch');
      Status(sl.Text);
      b := h.HTTPMethod('POST', 'https://v1.api.cloud.symless.com/profile/switch');
      CheckTrue(b, 'POST https://v1.api.cloud.symless.com/profile/switch');
      if b then begin
         sl.Clear;
         h.Document.Seek(0, soFromBeginning);
         sl.LoadFromStream(h.Document);
         Status(sl.Text);
      end;
      CheckEquals(200, h.ResultCode);
   finally
      h.Free;
      sl.Free;
   end;
end;

procedure TTestCaseSynergyCloudV1.TestQueryProfileUpdate;
var
   h: THTTPSend;
   sl: TStringList;
   b: boolean;
begin
   h := THTTPSend.Create;
   sl := TStringList.Create;
   sl.Add('{ "profile": { "name": "default" }, "screen": { "ipList": "192.168.60.129", "name": "DESKTOP-G628KTS", "status": "Disconnected" }}');
   try
      h.Document.Seek(0, soFromBeginning);
      h.Document.SetSize(0);
      sl.SaveToStream(h.Document);
      Status('https://v1.api.cloud.symless.com/profile/update');
      Status(sl.Text);
      b := h.HTTPMethod('POST', 'https://v1.api.cloud.symless.com/profile/update');
      CheckTrue(b, 'POST https://v1.api.cloud.symless.com/profile/update');
      if b then begin
         sl.Clear;
         h.Document.Seek(0, soFromBeginning);
         sl.LoadFromStream(h.Document);
         Status(sl.Text);
      end;
      CheckEquals(200, h.ResultCode);
   finally
      h.Free;
      sl.Free;
   end;
end;

procedure TTestCaseSynergyCloudV1.TestQueryScreenUpdate;
var
   h: THTTPSend;
   sl: TStringList;
   b: boolean;
begin
   h := THTTPSend.Create;
   sl := TStringList.Create;
   sl.Add('{ "id": 2062, "name": "DESKTOP-G628KTS", "status": "Connected" }');
   try
      h.Document.Seek(0, soFromBeginning);
      h.Document.SetSize(0);
      sl.SaveToStream(h.Document);
      Status('https://v1.api.cloud.symless.com/screen/update');
      Status(sl.Text);
      b := h.HTTPMethod('POST', 'https://v1.api.cloud.symless.com/screen/update');
      CheckTrue(b, 'POST https://v1.api.cloud.symless.com/screen/update');
      if b then begin
         sl.Clear;
         h.Document.Seek(0, soFromBeginning);
         sl.LoadFromStream(h.Document);
         Status(sl.Text);
      end;
      CheckEquals(200, h.ResultCode);
   finally
      h.Free;
      sl.Free;
   end;
end;


initialization
   TestFramework.RegisterTest(TTestCaseSynergyCloudV1.Suite);
end.
