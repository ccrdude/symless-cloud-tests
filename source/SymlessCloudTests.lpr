program SymlessCloudTests;

{$mode objfpc}{$H+}

uses
   Classes,
   Forms,
   Interfaces,
   TestFramework,
   GUITestRunner,
   SymlessCloudV1Tests;

{$R *.res}

begin
   Application.Title := 'Symless Synergy Cloud Tests';
   Application.Initialize;
   RunRegisteredTests;
end.
